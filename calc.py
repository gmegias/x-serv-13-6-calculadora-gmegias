def sumar(n1: int, n2: int) -> int:
    return n1 + n2

def restar(n1: int, n2: int) -> int:
    return n1 - n2

print("El resultado de sumar 1 y 2 es: " + str(sumar(1,2)))
print("El resultado de sumar 3 y 4 es: " + str(sumar(3,4)))
print("El resultado de restar 6 y 5 es: " + str(restar(6,5)))
print("El resultado de restar 8 y 7 es: " + str(restar(8,7)))